﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

[CustomEditor(typeof(Transform)), CanEditMultipleObjects]
public class Snap : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        //if (!Application.isPlaying)
        //{
        //    foreach (Transform t in targets)
        //    {
        //        Transform trans = t;
        //        trans.position = SnapTransform(trans.position);
        //    }
        //}
    }

    private Vector3 SnapTransform(Vector3 a_vec)
    {
        Vector3 vec = new Vector3
        {
            x = Mathf.Round(a_vec.x),
            y = Mathf.Round(a_vec.y),
            z = a_vec.z
        };
        return vec;
    }
}