﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Villager : MonoBehaviour
{
    SpriteRenderer m_renderer;

    void Awake()
    {
        m_renderer = gameObject.GetComponent<SpriteRenderer>();
    }
    void Update()
    {

    }

    public void Create(Sprite a_sprite, int a_zIndex)
    {
        m_renderer.sprite = a_sprite;
        m_renderer.sortingOrder = a_zIndex;
    }
}