﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    public GameObject m_sectionContainer, m_sliceContainer;
    public int m_zIndex;
    public int m_numberOfSlices, m_sizeOfSlice;
    public int m_numberOfSlicesPerSection;

    public GameObject m_slicePrefab;
    List<Slice> m_slices;
    List<Section> m_sections;

    public Map()
    {
        m_sections = new List<Section>();
        m_slices = new List<Slice>();
    }

    void Start()
    {

    }
    void Update()
    {

    }

    public void GenerateMap()
    {
        ClearMap();
        CreateSections();
        CreateSlices();
    }

    public void CreateSections()
    {
        Sprite sprite = Resources.Load<Sprite>("Sprites/Map/A_00_00");
        int numberOfSections = m_numberOfSlices / m_numberOfSlicesPerSection;
        Width = m_numberOfSlices * m_sizeOfSlice;
        for (int i = 0; i < numberOfSections; i++)
        {
            GameObject obj = new GameObject("Section" + i.ToString());
            Section section = obj.AddComponent<Section>();
            section.Create(sprite, "A_00_00", m_zIndex);
            m_sections.Add(section);
            obj.transform.parent = m_sectionContainer.transform;
            obj.transform.localPosition = new Vector3(0, 0, 0);
            obj.transform.Translate(new Vector3(i * sprite.rect.width + (sprite.rect.width * 0.5f), 32, 0));
        }
    }
    public void CreateSlices()
    {
        for (int i = 0; i < m_numberOfSlices; i++)
        {
            GameObject obj = Instantiate(m_slicePrefab, m_sliceContainer.transform);
            obj.name = "Slice_" + i.ToString();
            Slice c = obj.GetComponent<Slice>();
            c.Renderer.sortingOrder = m_zIndex + 1;
            c.Position = (m_sizeOfSlice * i) + (int)(c.Renderer.sprite.rect.width * 0.5f);
            c.transform.Translate(new Vector3(c.Position, 0, 0));
            m_slices.Add(c);
        }
        for (int i = 0; i < m_numberOfSlices; i++)
        {
            if (i != 0)
            {
                m_slices[i].LeftSlice = m_slices[i - 1];
            }

            if (i != m_numberOfSlices - 1)
            {
                m_slices[i].RightSlice = m_slices[i + 1];
            }
        }
    }

    public void ClearMap()
    {
        for (int i = m_sections.Count - 1; i >= 0; i--)
        {
            Destroy(m_sections[i]);
        }
        m_sections.Clear();
        Width = 0.0f;
    }
    public Slice GetCurrentSlice(float a_position)
    {
        if (a_position < 0 || a_position >= Width + 1)
            return null;

        a_position /= m_sizeOfSlice;
        int startIndex = Mathf.FloorToInt(a_position);
        startIndex = Mathf.Clamp(startIndex, 0, m_slices.Count - 1);
        return m_slices[startIndex];
    }
    public List<Slice> GetEmptySlices(float a_position)
    {
        if (a_position < 0 || a_position > Width + 1)
            return null;

        a_position /= m_sizeOfSlice;

        int startIndex = Mathf.FloorToInt(a_position);
        startIndex = Mathf.Clamp(startIndex, 0, m_slices.Count - 1);
        if (m_slices[startIndex].Building != null)
        {
            return null;
        }

        List<Slice> slices = new List<Slice>();
        slices.Add(m_slices[startIndex]);

        Slice leftSlice = m_slices[startIndex].LeftSlice;
        Slice rightSlice = m_slices[startIndex].RightSlice;
        int count = 1;

        while ((leftSlice != null || rightSlice != null) && count < 3) //3 = max
        {
            if (leftSlice != null)
            {
                if (leftSlice.Building == null)
                {
                    slices.Insert(0, leftSlice);
                    leftSlice = leftSlice.LeftSlice;
                    count++;
                }
                else
                {
                    leftSlice = null;
                }
            }

            if (rightSlice != null)
            {
                if (rightSlice.Building == null)
                {
                    slices.Insert(slices.Count, rightSlice);
                    rightSlice = rightSlice.RightSlice;
                    count++;
                }
                else
                {
                    rightSlice = null;
                }
            }
        }
        return slices;
    }

    public float Width { get; private set; }
}