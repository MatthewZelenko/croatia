﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    public Dictionary<string, Type> m_buildingTypes;
    public Dictionary<string, List<Building>> m_buildings;

    public BuildingManager()
    {
        m_buildings = new Dictionary<string, List<Building>>();
        m_buildingTypes = new Dictionary<string, Type>
        {
            { "house", typeof(House) },
            { "farm", typeof(Farm) }//,
            //{ "tower", typeof(Tower) }
        };
    }

    //Returns created building
    public Building CreateBuilding(string a_type)
    {
        GameObject buildingObj = new GameObject(a_type);
        Type t = m_buildingTypes[a_type];
        Building b = buildingObj.AddComponent(t) as Building;
        b.ID = AddBuilding(b);
        buildingObj.transform.parent = transform;
        b.Renderer.sprite = Resources.Load<Sprite>("Sprites/Map/" + a_type);
        buildingObj.transform.localPosition = new Vector3();
        return b;
    }
    public void DestroyBuilding(Building a_building)
    {
        RemoveBuilding(a_building);
        Destroy(a_building.gameObject);
    }

    //Returns ID
    int AddBuilding(Building a_building)
    {
        if (!m_buildings.ContainsKey(a_building.BuildingType))
        {
            m_buildings.Add(a_building.BuildingType, new List<Building>());
        }
        m_buildings[a_building.BuildingType].Add(a_building);
        return m_buildings[a_building.BuildingType].Count - 1;
    }

    void RemoveBuilding(Building a_building)
    {
        if (m_buildings.ContainsKey(a_building.BuildingType))
        {
            List<Building> buildings = m_buildings[a_building.BuildingType];
            if (buildings.Count > 1)
            {
                SwapAndPop(buildings, a_building.ID);
            }
            else if (buildings.Count == 1)
            {
                m_buildings[a_building.BuildingType].Clear();
            }
        }
    }
    void SwapAndPop(List<Building> a_buildings, int a_removed)
    {
        a_buildings[a_removed] = a_buildings[a_buildings.Count - 1];
        a_buildings[a_removed].ID = a_removed;
        a_buildings.RemoveAt(a_buildings.Count - 1);
    }
}