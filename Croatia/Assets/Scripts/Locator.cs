﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Locator
{
    static Locator m_instance;

    public Locator()
    {

    }

    public void Awake()
    {
        UIManager = GameObject.FindObjectOfType<UIManager>();
        Map = GameObject.FindObjectOfType<Map>();
        WorldManager = GameObject.FindObjectOfType<WorldManager>();
        BuildingManager = GameObject.FindObjectOfType<BuildingManager>();
    }

    public UIManager UIManager { get; private set; }
    public Map Map{ get; private set; }
    public WorldManager WorldManager { get; internal set; }
    public BuildingManager BuildingManager{ get; internal set; }

    public static Locator Instance
    {
        get
        {
            if (m_instance == null)
            {
                m_instance = new Locator();
            }
            return m_instance;
        }
    }
}