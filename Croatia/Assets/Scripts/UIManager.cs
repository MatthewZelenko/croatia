﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using UnityEngine.Events;

public class UIManager : MonoBehaviour
{
    public enum BubbleMenus
    {
        MAIN,
        BUILD,
        EDIT
    }
    BubbleMenus m_currentMenu;
    int m_active;

    public Canvas m_canvas;

    public GameObject m_bubbleParent;
    public BubbleMenuItem m_bubblePrefab;
    public List<BubbleMenuItem> m_pooledBubbles;
    public Player m_player;

    public List<Slice> m_activeSlices;

    public UIManager()
    {
        m_active = -1;
        m_currentMenu = BubbleMenus.MAIN;
        m_activeSlices = new List<Slice>();
    }
    private void Start()
    {
        m_player = FindObjectOfType<Player>();

        m_pooledBubbles = new List<BubbleMenuItem>();
        //Max bubble limit
        for (int i = 0; i < 15; i++)
        {
            m_pooledBubbles.Add(Instantiate(m_bubblePrefab));
            m_pooledBubbles[i].transform.parent = m_bubbleParent.transform;
            m_pooledBubbles[i].gameObject.SetActive(false);
        }
        m_bubbleParent.SetActive(false);
    }

    private void Update()
    {
        //TODO::Change to swipe
        if (Input.GetMouseButtonUp(0))
        {
            if (m_active == -1)
            {
                OpenBubbleMenu(BubbleMenus.MAIN);
            }
        }
        if (m_active == 0)
        {
            m_active = -1;
        }
    }
    private void FixedUpdate()
    {
        if (m_active == 1)
        {
            if (m_player.HasMoved)
                CloseBubbleMenu();
            else
                m_canvas.transform.position = new Vector3(m_player.transform.position.x, m_canvas.transform.position.y, m_canvas.transform.position.z);
        }
    }

    public void OpenBubbleMenu(BubbleMenus a_menu)
    {
        CloseBubbleMenu();
        m_active = 1;
        m_currentMenu = a_menu;

        m_canvas.transform.position = new Vector3(m_player.transform.position.x, m_canvas.transform.position.y, m_canvas.transform.position.z);
        m_bubbleParent.SetActive(true);
        LoadBubbles();
    }
    public void CloseBubbleMenu()
    {
        //TODO::Close Animation
        for (int i = 0; i < m_pooledBubbles.Count; i++)
        {
            m_pooledBubbles[i].gameObject.SetActive(false);
        }
        m_active = 0;
        m_bubbleParent.SetActive(false);

        if (m_activeSlices != null)
        {
            foreach (Slice slice in m_activeSlices)
            {
                slice.Activate(false);
            }
            m_activeSlices = null;
        }
    }

    public void Build(string a_type)
    {
        Building b = Locator.Instance.BuildingManager.CreateBuilding(a_type);
        b.transform.Translate(new Vector3(m_activeSlices[Mathf.FloorToInt(m_activeSlices.Count / 2)].transform.position.x, 0, 0));
        foreach (Slice s in m_activeSlices)
        {
            s.PlaceBuilding(b);
        }

        Debug.Log("Built: " + a_type);
        CloseBubbleMenu();
    }

    void LoadBubbles()
    {
        List<Sprite> images = Resources.LoadAll<Sprite>("Sprites/UI/Popup Menu").ToList();

        Slice CurrentSlice = Locator.Instance.Map.GetCurrentSlice(m_player.transform.position.x);

        int numOfBubbles = 0;

        if (m_currentMenu == BubbleMenus.MAIN)
        {
            m_pooledBubbles[0].name = "popup_settings";
            m_pooledBubbles[0].gameObject.SetActive(true);
            m_pooledBubbles[0].m_image.sprite = images.Find((Sprite sprite) =>
            {
                if (sprite.name == "popup_settings")
                    return true;
                return false;
            });
            m_pooledBubbles[0].m_button.onClick.RemoveAllListeners();
            m_pooledBubbles[0].m_button.onClick.AddListener(() => { Debug.Log("Settings Pushed"); CloseBubbleMenu(); });
            numOfBubbles++;
            if (CurrentSlice.Building)
            {
                m_pooledBubbles[1].name = "popup_edit";
                m_pooledBubbles[1].gameObject.SetActive(true);
                m_pooledBubbles[1].m_image.sprite = images.Find((Sprite sprite) =>
                {
                    if (sprite.name == "popup_edit")
                        return true;
                    return false;
                });
                m_pooledBubbles[1].m_button.onClick.RemoveAllListeners();
                m_pooledBubbles[1].m_button.onClick.AddListener(() => { Debug.Log("edit Pushed"); OpenBubbleMenu(BubbleMenus.EDIT); });
            }
            else
            {
                m_pooledBubbles[1].name = "popup_build";
                m_pooledBubbles[1].gameObject.SetActive(true);
                m_pooledBubbles[1].m_image.sprite = images.Find((Sprite sprite) =>
                {
                    if (sprite.name == "popup_build")
                        return true;
                    return false;
                });
                m_pooledBubbles[1].m_button.onClick.RemoveAllListeners();
                m_pooledBubbles[1].m_button.onClick.AddListener(() => { Debug.Log("build Pushed"); OpenBubbleMenu(BubbleMenus.BUILD); });
            }
            numOfBubbles++;
        }
        else if (m_currentMenu == BubbleMenus.BUILD)
        {
            string s = "Data/" + m_currentMenu.ToString().ToLower();
            TextAsset textAsset = Resources.Load<TextAsset>(s);
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.Indented;


            Dictionary<string, BuildBubble> availableBubbles = JsonConvert.DeserializeObject<Dictionary<string, BuildBubble>>(textAsset.text, settings);
            m_activeSlices = Locator.Instance.Map.GetEmptySlices(m_player.transform.position.x);
            if (m_activeSlices == null)
            {
                CloseBubbleMenu();
                return;
            }
            foreach (Slice slice in m_activeSlices)
            {
                slice.Activate(true);
            }
            int count = 0;
            foreach (KeyValuePair<string, BuildBubble> kv in availableBubbles)
            {
                //If bubble should not be here remove.
                if (kv.Value.m_minNumOfSlices > m_activeSlices.Count || kv.Value.m_age > Locator.Instance.WorldManager.Age)
                {
                    availableBubbles.Remove(kv.Key);
                    continue;
                }
                else
                {
                    if (count >= m_pooledBubbles.Count)
                    {
                        Debug.Log("Too Many Bubbles. Increase pool size.");
                        return;
                    }
                    //Find and add bubble
                    m_pooledBubbles[count].gameObject.SetActive(true);
                    m_pooledBubbles[count].m_image.sprite = images.Find((Sprite sprite) =>
                    {
                        if (sprite.name == "popup_build_" + kv.Key)
                            return true;
                        return false;
                    });
                    m_pooledBubbles[count].name = "popup_build_" + kv.Key;
                    m_pooledBubbles[count].m_button.onClick.RemoveAllListeners();
                    m_pooledBubbles[count].m_button.onClick.AddListener(() => { Build(kv.Key); });
                    numOfBubbles++;
                    count++;
                }
            }
        }
        else if (m_currentMenu == BubbleMenus.EDIT)
        {
            CloseBubbleMenu();
        }
        for (int i = 0; i < numOfBubbles; i++)
        {
            m_pooledBubbles[i].transform.localPosition = new Vector3(i * 48 - (numOfBubbles * 0.5f * 48 - 24), 32, 0);
        }
    }
}

public class BuildBubble
{
    [UnityEditor.MenuItem("Custom/CreateBubbleTemplate")]
    public static void CreateBubbleTemplate()
    {
        Dictionary<string, BuildBubble> bubbles = new Dictionary<string, BuildBubble>();
        for (int i = 0; i < 8; i++)
        {
            bubbles["TemplateName_" + i.ToString()] = new BuildBubble(2, 0);
        }

        if (!Directory.Exists("Assets/Resources/Data/"))
        {
            Directory.CreateDirectory("Assets/Resources/Data/");
        }
        string jsonString = JsonConvert.SerializeObject(bubbles, Formatting.Indented);
        File.WriteAllText("Assets/Resources/Data/build.txt", jsonString);
    }

    public BuildBubble(sbyte a_numOfSlices, sbyte a_age)
    {
        m_minNumOfSlices = a_numOfSlices;
        m_age = a_age;
    }
    //1 - 3
    public sbyte m_minNumOfSlices;
    //Wood, Stone, Gold
    public sbyte m_age;
}