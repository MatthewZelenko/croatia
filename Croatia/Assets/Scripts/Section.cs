﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Just a visual object 
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class Section : MonoBehaviour
{
    SpriteRenderer m_renderer;

    public Section()
    {

    }

    void Awake()
    {
        m_renderer = gameObject.GetComponent<SpriteRenderer>();
    }
    void Update()
    {

    }

    public void Create(Sprite a_sprite, string a_code, int a_zIndex)
    {
        m_renderer.sprite = a_sprite;
        m_renderer.sortingOrder = a_zIndex;
        Code = a_code;
    }

    public void SetBuilding(Building a_building) { Building = a_building; }

    public Building Building { get; private set; }
    public string Code { get; private set; }
}