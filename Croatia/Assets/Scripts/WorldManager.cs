﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldManager : MonoBehaviour
{
    public Map m_map;
    public Population m_population;
    public TrackCamera m_camera;

    public WorldManager()
    {
        Age = 0;
    }

    private void Awake()
    {
        Locator.Instance.Awake();
    }
    void Start()
    {
        m_map.GenerateMap();
        m_population.GenerateVillagers(6, m_map.Width * 0.5f);
        m_camera.transform.Translate(new Vector3(m_map.Width * 0.5f, 0, 0));
        m_camera.Create(m_population.m_player);
    }
    void Update()
    {

    }

    public int Age { get; private set; }

}