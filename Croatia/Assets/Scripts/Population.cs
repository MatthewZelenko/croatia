﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Population : MonoBehaviour
{
    public Player m_player;

    List<Villager> m_villagers;
    public int m_zIndex;

    public Population()
    {
        m_villagers = new List<Villager>();
    }

    void Start()
    {

    }
    void Update()
    {

    }

    public void GenerateVillagers(int a_numOfVillagers, float a_xPosition)
    {
        ClearPopulation();

        Sprite sprite = Resources.Load<Sprite>("Sprites/Characters/Sailor");
        for (int i = 0; i < a_numOfVillagers; i++)
        {
            GameObject obj = new GameObject("Villager" + i.ToString());
            Villager villager = obj.AddComponent<Villager>();
            villager.Create(sprite, m_zIndex);
            obj.transform.parent = transform;
            obj.transform.localPosition = new Vector3(0, 0, 0);
            obj.transform.Translate(new Vector3(a_xPosition + 16 * i, 32 + 64, 0));

            m_villagers.Add(villager);
        }


        GameObject playerObj = new GameObject("Player");
        m_player = playerObj.AddComponent<Player>();
        playerObj.transform.parent = transform;
        playerObj.transform.localPosition = new Vector3(0, 0, -10);
        playerObj.transform.Translate(new Vector3(a_xPosition, 32 + 64, 0));
        m_player.Create(Resources.Load<Sprite>("Sprites/Characters/King"), m_zIndex + 1);
    }

    public void ClearPopulation()
    {
        for (int i = m_villagers.Count - 1; i >= 0; i--)
        {
            Destroy(m_villagers[i]);
        }
        m_villagers.Clear();
    }
}