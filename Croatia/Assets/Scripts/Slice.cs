﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// Holds information about buildings
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class Slice : MonoBehaviour
{
    public Slice()
    {

    }

    public void Awake()
    {
        Renderer = GetComponent<SpriteRenderer>();
    }

    public void PlaceBuilding(Building a_building)
    {
        Assert.IsNull(Building);
        Assert.IsNotNull(a_building);
        Building = a_building;
    }
    public void RemoveBuilding()
    {
        Building = null;
    }
    public void Activate(bool a_activate)
    {
        if (a_activate)
        {
            Renderer.color = new Color(Renderer.color.r, Renderer.color.g, Renderer.color.b, 1.0f);
        }
        else
        {
            Renderer.color = new Color(Renderer.color.r, Renderer.color.g, Renderer.color.b, 0.0f);
        }
    }

    public Building Building { get; private set; }
    public int Position { get; internal set; }

    public Slice LeftSlice { get; set; }
    public Slice RightSlice { get; set; }
    public SpriteRenderer Renderer { get; private set; }
}