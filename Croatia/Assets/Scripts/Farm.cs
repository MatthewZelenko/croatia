﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Farm : Building
{
    public Farm() : base("farm")
    {
    }

    void Awake()
    {
        Init();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
