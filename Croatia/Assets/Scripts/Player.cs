﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Player : MonoBehaviour
{
    SpriteRenderer m_renderer;

    int m_direction;
    public float m_speed = 64;
    Slice m_currentSlice;

    void Awake()
    {
        m_renderer = GetComponent<SpriteRenderer>();
    }
    void Start()
    {
        m_currentSlice = Locator.Instance.Map.GetCurrentSlice(transform.position.x);
    }
    void Update()
    {
        m_direction = 0;
        HasMoved = false;
        if (Input.GetKey(KeyCode.A))
        {
            m_direction = -1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            m_direction = 1;
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            m_speed *= 4.0f;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            m_speed = 64.0f;
        }
    }
    private void FixedUpdate()
    {
        if (m_direction != 0)
        {
            Vector2 newPosition = transform.position + new Vector3(m_speed * m_direction * Time.fixedDeltaTime, 0, 0);
            Map map = Locator.Instance.Map;
            Slice newSlice = map.GetCurrentSlice(newPosition.x);
            if (newSlice != null)
            {
                m_currentSlice = newSlice;
            }
            else
            {
                if (m_currentSlice.LeftSlice == null)
                {
                    newPosition.x = 0.0f;
                }
                else if (m_currentSlice.RightSlice == null)
                {
                    newPosition.x = m_currentSlice.Position + map.m_sizeOfSlice * 0.5f;
                }
                else
                {
                    Debug.Log("NO SLICE!!!!!! LEFT OR RIGHT!!!!!!");
                }
            }
            transform.position = newPosition;
            HasMoved = true;
        }
        else
        {

        }
    }

    public void Create(Sprite a_sprite, int a_zIndex)
    {
        m_renderer.sprite = a_sprite;
        m_renderer.sortingOrder = a_zIndex;
    }

    public bool HasMoved { get; private set; }
}