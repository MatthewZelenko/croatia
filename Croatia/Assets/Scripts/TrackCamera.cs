﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class TrackCamera : MonoBehaviour
{
    Camera m_camera;
    Player m_player;

    public TrackCamera()
    {

    }

    public void Create(Player a_player)
    {
        m_player = a_player;
    }
    void Awake()
    {
        m_camera = GetComponent<Camera>();
    }
    void Update()
    {

    }
    private void FixedUpdate()
    {
        if (m_player != null && m_player.HasMoved)
        {
            transform.position = new Vector3(m_player.transform.position.x, transform.position.y, transform.position.z);
        }
    }
}