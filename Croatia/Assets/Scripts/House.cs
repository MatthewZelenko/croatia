﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : Building
{
    ResidentComponent residentComponent;


    public House() : base("house")
    {
    }

    void Awake()
    {
        Init();
        residentComponent = gameObject.AddComponent<ResidentComponent>();
        residentComponent.Create(2);
    }
    void Start()
    {

    }
    void Update()
    {

    }
}