﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResidentComponent : MonoBehaviour
{
    public ResidentComponent()
    {
        Residents = new List<Villager>();
    }

    void Start()
    {

    }
    void Update()
    {

    }

    public void Create(int a_numberOfResidents)
    {
        MaxResidents = a_numberOfResidents;
    }

    public bool AddResident(Villager a_villager)
    {
        if (IsFull)
        {
            return false;
        }
        Residents.Add(a_villager);
        return true;
    }
    public void RemoveResident(Villager a_villager)
    {
        for (int i = 0; i < Residents.Count; i++)
        {
            if (Residents[i] == a_villager)
            {
                Residents.RemoveAt(i);
                return;
            }
        }
    }
    public void ClearResidents()
    {
        Residents.Clear();
    }

    public List<Villager> Residents { get; private set; }
    public int MaxResidents { get; private set; }
    public bool IsFull { get { return Residents.Count >= MaxResidents; } }
}