﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Building : MonoBehaviour
{
    public Building(string a_type)
    {
        BuildingType = a_type;
    }
    

    public int ID { get; set; }
    public string BuildingType { get; private set; }

    protected void Init()
    {
        Renderer = GetComponent<SpriteRenderer>();
    }
    void Start()
    {

    }
    void Update()
    {

    }

    public List<Slice> m_activeSlices;
    public SpriteRenderer Renderer { get; set; }
}